# pianojacq

A - for now - browser (Chrome only, sorry) based piano tutoring program that allows you to use a midi keyboard and a computer to study playing the piano, organ, keyboard/synthesizer or harpsichord. It will also help you to learn sightreading.

For a working demonstration you can visit the website of the project:

 https://www.pianojacq.com/

The about page lives here:

 https://www.pianojacq.com/about.html

This project is open to outside contributors. But there are a couple of
ground rules:

- You may contribute, but there is no obligation to incorporate your
  contribution.

- I already have a job, while this project is very dear to me it is meant
  to be fun, both for the users and for me. If you are rude, annoying,
  attempt to take over the project or irritate me I will ruthlessly
  kick you off. So be kind to me and any other contributors.

- I am interested in

  - bug reports
  - help in the domains of music and user experience
  - code to fix bugs
  - tests
  - code to add functionality

  Feel free to contact me if you want to contribute something more
  substantial.

- Attempts to pull the project in a direction where I don't want to go
  will be ignored.

- Attempts to introduce new tools, libraries, frameworks or other structural
  elements without prior discussion and approval will very likely be ignored.

- Make sure to check out the license file before attempting to contribute,
  yes, this project is 'open' in the sense that you can read the code,
  download and change it. But you are not allowed to 'fork' the code for
  any other purpose than to work on it for your personal use or to contribute
  to the project, the Copyright rests with me and/or my company and it will
  likely remain that way. I've seen enough open source authors and project
  managers or maintainers come to grief over their own creations that I intend
  to remain in control of this one in such a way that I can guarantee that
  everybody involved plays nice with each other.

Enjoy!

   Jacques Mattheij
   jacques@modularcompany.com

## Getting Started

If you don't have a MIDI keyboard, you can use a virtual one. Several such tools exist,
including [vkeybd](https://github.com/tiwai/vkeybd) for Linux machines.

## Contributing

### Style

Currently, we are not using any Linter to enforce a style guide. A handful of conventions
should be used to keep consistent, and this list will evolve over time.

- Use spaces (` `) instead of hard tabs (`\t`). One indentation is 8 spaces.
- Vanilla javascript. No toolchain, no Babel precompilation, etc. Not for now at least.
- There is a config file for ESLint at the root of the repository. ESLint can be
  used for noticing potential mistakes and coding style issues but its use is
  optional and its report should be used as a guideline and not as something
  that should block the CI/CD or prevent a merge. Be aware that the ESLint
  configuration is experimental and may be refined in the future.
