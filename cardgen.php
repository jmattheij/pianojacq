<?php

// quick and dirty PHP flash card generator for the intervals, chords and scales

$interval_names = [ 'P0', 'm2', 'M2', 'm3', 'M3', 'P4', 'A4', 'P5', 'm6', 'M6', 'm7', 'M7', 'P8' ];

//                         P0 m2 M2 m3 M3 P4 A4 P5 m6 M6 m7 M7 P8

$interval_difficulties = [  0, 5, 4, 0, 0, 2, 3, 1, 6, 6, 7, 7, 0 ];

// compute a difficulty from a given interval and the location of the root
// note, that way you can order these by difficulty level for presentation
// to the user.

// More choices is more difficult than fewer choices, further away from C4 
// is more difficult than closer to it and some intervals are harder to
// distinguish than others.

function difficulty($interval, $root, $nchoices)

{
        global $interval_difficulties;

        $dist = abs(60 - $root);

        return $nchoices + $interval_difficulties[$interval] * 2 + $dist;
}

// generate all possible cards

for ($root = 21; $root < 109; $root++) {
        for ($interval = 1; $interval < 13; $interval++) {
                for ($nchoices = 2; $nchoices < 13; $nchoices++) {
                        $d = difficulty($interval, $root, $nchoices);

                        // ensure we stay within a range that can be
                        // represented by a regular 88 key piano

                        if (($root + $interval) < 109) {
                                $cards[] = [ $d, $root, $interval, $nchoices ];
                        }
                }
        }
}

// For each interval that is the subject of the test we
// can add other intervals based on how many choices are
// available to expand the difficulty. Because some
// combinations are harder than others we allow for
// re-ordering

// P0 m2 M2 m3 M3 P4 A4 P5 m6 M6 m7 M7 P8
//  0  1  2  3  4  5  6  7  8  9 10 11 12

$interval_choices = [
        [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],       // P0; not used
        [ 1, 2,12, 4, 7, 3, 5, 6, 8, 9,10,11, 0],       // m2
        [ 2, 1,12, 4, 7, 3, 5, 6, 8, 9,10,11, 0],       // M2
        [ 3,12, 4, 7, 5, 2, 6, 1, 8, 9,10,11, 0],       // m3
        [ 4,12, 2, 7, 5, 2, 6, 1, 8, 9,10,11, 0],       // M3
        [ 5, 7,12, 4, 3, 6, 2, 1, 8, 9,10,11, 0],       // P4
        [ 6,12, 5, 7, 4, 3, 2, 1, 8, 9,10,11, 0],       // A4
        [ 7,12, 5, 4, 3, 6, 2, 1, 8, 9,10,11, 0],       // P5
        [ 8, 3,12, 4, 7, 5, 1, 2, 6, 9,10,11, 0],       // m6
        [ 9, 8,12, 3, 4, 7, 5, 1, 2, 6,10,11, 0],       // M6
        [10,11,12, 3, 4, 7, 5, 1, 2, 6, 8, 9, 0],       // m7
        [11,10,12, 3, 4, 7, 5, 1, 2, 6, 8, 9, 0],       // M7
        [12, 4, 7, 3, 5, 6, 1, 2, 8, 9,10,11, 0]
]; 
   
$id = 0;

function show_card($card)

{
        global $id;
        global $interval_names;
        global $interval_choices;

        $root = $card[1];
        $interval = $card[2];

        $second = $card[1] + $card[2];
        $nchoices = $card[3];

        $choices = "";

        for ($c=0;$c<$nchoices;$c++) {
                if (strlen($choices) > 0) {
                        $choices = $choices . ",";
                }

                $choices = $choices . "'" . $interval_names[$interval_choices[$interval][$c]] . "'";
        }

        // the answer is always the first option in the list but we specify it
        // anyway because that gives a lot of flexibility for other cards.

        $c = "{ id: $id, et_mode: 0, notes: [$root, $second], choices: [$choices], answer: 0 }, ";

        echo("$c\n");

        $id++;
}

function cmp($a, $b)

{
        return $a[0] - $b[0];
}

usort($cards, "cmp");

// now sort the cards by difficulty

// print_r($cards);

// and output the set of cards

$id = 0;

echo("var cards = [\n");

foreach ($cards as $card)

{
        show_card($card);
}

echo("];\n");

