//
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 
// 
// This is the persistence layer, it builds on Lovefield and IndexDB
//

// A Warning on using this for those who are used to other databases.
// There is a bunch of caching between your function calls to lovefield
// and the storage engine, on top of that not all functions that you
// have executed see their effects reflected across all of the underlying
// layers. This can have some very surprising - and bad - effects, such
// as inserting a record, querying that same table and not finding your
// record there. Refreshing the page will invariably turn it up so it
// definitely does get saved. There are a number of workarounds in the
// code to deal with those effects, the regular method of adding
// promises to asynchronous updates are not going to solve this problem.

// Deletion has similar issues, if you delete something there is no guarantee
// that it won't turn up again on a subsequent query. 

// See also:

// https://github.com/google/lovefield/blob/master/docs/spec/05_transaction.md#52-implicit-transaction
// https://stackoverflow.com/questions/15492314/why-is-indexeddb-not-showing-new-entries-in-chrome
// https://stackoverflow.com/questions/15151242/running-code-only-after-an-object-is-updated-in-indexeddb-particularly-in-chrom

// Of course it makes perfect sense to make your database 
// ascynchronous. /not.

// set up the database schema and the database connection

// update this version number whenever you add or change a table

const dbversion = 21;

var db_connected = false;

// Because it's impossible to query whether or not a database
// exists before opening it we are stuck with the old name,
// even for new installations. 

var schemaBuilder = lf.schema.create('practicekeyboard', dbversion);

var database;

function db_connect()

{
schemaBuilder.connect().then(function(db) {
        database = db;

        db_now_connected();
});
}

