//
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 
//
// This file contains everything that has to do with the realtime
// aspect of the program, the piece animation and the processing 
// of midi input events as well as the playing of grayed out notes.
//
// Maybe a better name for this file would be 'practice.js'?
//

// The drivers for any kind of activity for this file are:
// - the timer that drives the movement of the graphical representation
//   of the piece scrolling left to right as stored in a number of canvases 
// - the reception of midi messages
//
// Each of these has a handler. 

//
// This is probably with the exception of the vexflow stuff the hardest
// code in this project, be careful making changes, the results can be
// quite unexpected so read carefully to get some context, almost all
// of the code here runs in the context of some event handler and is
// very time critical, a slight slowdown could make the program entirely
// unusable. 
//

// TODO: many references to the 'piece' singleton here, it would be more elegant if we 
// could pass that in somehow rather than using the global

// valid states for #state
// these should be made private to the class

// JM->AF: there are a bunch of ties between this code and 'piece' that feel very tenuous and that I would like to get rid of to
//         make this class less cluttered and to make it more testable. I've marked a bunch of those spots with 'TODO', have a 
//         look to see if you can find a better way to structure this. 
// JM->AF: another JS issue, I can't seem to find the magic incantation required to make a private const (const this.#stopped = 1;)

const stopped = 1;       // the user has commanded a pause or the program has not started yet
const waiting = 2;      // we are waiting for the user to play notes (is this different enough from paused to warrant another state?)
const running = 3;      // the animation is scrolling, any user played notes are ahead of where they should be
const finished = 4;     // at the end of a practice 

class Realtime {

        #interval_timer;

        // state indicates the state of the scroller and if activated the player
        // and keeps the music and the visual representation of the piece synchronized 
        // and animated. When 'stopped' we are typically waiting for the user to press 
        // 'play' or to play notes. 

        #state;

        #piece_xpos;
        #ms_per_tick;
        #bound_one_tick;
        #had_an_error;

        #stopped;

        // here we keep track of how long we were waiting after the
        // notes we expected turned lightgreen. This can then be
        // used to determine whether we will make them lightgreen
        // (late) or regular green (on time). Technically any delay
        // is too much but the display moves quite fast and it is 
        // too hard to get the notes to hit properly every time.
        // especially true because the scrolling can be quite jumpy
        // which means you don't even get a single frame to try to 
        // hit the note with perfect timing.

        #ticks_waiting;

        // where we currently are in the piece
        #current_tick;

        // streak counts how many times in a row there were no errors

        #streak;

        constructor ()

        {
                this.#interval_timer = null;
                this.#ms_per_tick = 0;    // this is used to determine how long we are in the 'waiting' state during a practice run
                this.#piece_xpos = 0;    // the position of the left most canvas (even if we are not actually moving it we still keep it relative to that)
                this.#had_an_error = false;       // indicates that during the playing of these note(s) the player made an error

                this.#current_tick = -1;         // this serves as a pre-roll of sorts

                this.#state = stopped;

                this.#ticks_waiting = 0;

                // bind the 'this' context to the one_tick function to ensure
                // it can access all of the private variables and functions
                // of the class instance

                this.#bound_one_tick = this.#one_tick.bind(this);

                this.#streak = 0;
        }

        is_stopped()

        {
                return this.#state == stopped;
        }

        is_running()

        {
                return this.#state == running;
        }

        is_waiting()

        {
                return this.#state == waiting;
        }

        is_finished()

        {
                return this.#state == finished;
        }

        // Maybe this should go to config.js? or even a karma.js
        // update karma count; update karma and streak displays
        // TODO

        karma_update(n)

        {
	        if (config.karma > 0) {
		        config.karma += n;
	        }
	        else {
                        // this should only happen during initialization and never afterwards
                        // if for whatever reason this ever happens again then the student will
                        // lose all of their accumulated karms.
		        config.karma = n;
	        }

	        $("#karma").html("karma: " + config.karma);

                // this really should be in a 'streak_update' function TODO

	        $("#streak").html("streak: " + this.#streak);
        }

        #play_active_notes(x, hands)

        {
                // TODO: link to 'piece', can we do without this somehow?

                var notes = piece.notes_at_x(x);

                for (var i=0;i<notes.length;i++) {
                        // TODO hardcoded volume here
                        if (!notes[i].grayed) {
                                // the velocity value should really depend on the velocity that the
                                // user is currently playing their notes at
                                midiio.play_note(notes[i].midi, notes[i].duration, 64);
                        }
                }
        }

        // this will play all the notes the user can not or has 
        // chosen not to play on account of what they are practicing
        // (left, right) or because the limitations of their keyboard

        // this is used in following and automatic mode

        #play_grayed_notes(x)

        {
                if (this.is_waiting()) {
                        return;
                }

                // TODO: link to 'piece', can we do without this somehow

                var notes = piece.notes_at_x(x);

                if (typeof(notes) == "undefined") {
                        return;
                }

                for (var i=0;i<notes.length;i++) {
                        // TODO hardcoded volume here
                        if (notes[i].grayed) {
                                // we play these grayed notes at reduced volume so the player can hear themselves well
                                midiio.play_note(notes[i].midi, notes[i].duration * tempo_multiplier, 30);
                        }
                }
        }

        // check to see if a note is expected, whether it is grayed out or not does not matter
        // used to determine whether we should classify a note as an error
        // this is public because notecursors needs it too to determine the color of the notes
        // (notes that are not expected have their cursor colored red)

        expected(midinote, notes)

        {
                if (typeof(notes) == "undefined") {
                        return true;
                }

                for (var note of notes) {
                        // matching note
                        if (note.midi == midinote) {
                                return true;
                        }
                }

                // no matching note; or not grayed out.

                return false;
        }

        // a note is expected *and* it is not grayed out
        // used to determine whether the expected notes are all played

        #expected_active(midinote, notes)

        {
                if (typeof(notes) == "undefined") {
                        return true;
                }

                for (var note of notes) {
                        // matching note
                        if (note.midi == midinote && note.grayed == false) {
                                return true;
                        }
                }

                // no matching note; or not grayed out.

                return false;
        }

        // check how many notes of this particular spot are not grayed out

        #count_active_notes(notes)

        {
                var n = 0;

                if (typeof(notes) == "undefined") {
                        return 0;
                }

                for (var note of notes) {
                        if (!note.grayed) {
                                n++;
                        }
                }

                return n;
        }

        // a bad note is one that isn't in the expected array at all

        #count_bad_notes(sounding, expected_notes)

        {
                for (var i=0;i<sounding.length;i++) {
                        if (!this.expected(sounding[i], expected_notes)) {

                                // TODO link to piece, can we do without this?

                                if (!piece.allowed_to_sound(this.#current_tick, sounding[i])) {
                                        return true;
                                }
                                else {
                                        // this may lead to double strikes being ignored entirely
                                        // if true we may need to differentiate between stopped
                                        // mode and 'running' mode where we only excuse the
                                        // note if we are stopped to ensure that some kind of 
                                        // error is recorded
                                        // maybe a TODO ?

                                        // for now log this kind of mistake on the console
                                        // so we can see how often this happens

                                        console.log("pardoning a note that is still sounding");
                                }
                        }
                }

                return false;
        }

        // count the number of notes that are expected; not grayed 

        #count_good_notes(sounding, expected_notes)

        {
                var n = 0;

                for (var i=0;i<sounding.length;i++) {
                        if (this.#expected_active(sounding[i], expected_notes)) {
                                n++;
                        }
                }

                return n;
        }

        // check to see if the cursor is exactly on a set of notes to be checked
        // or played

        #on_hotspot(tick_x)

        {
                if (tick_x < this.#hairline_x()) {
                        return true;
                }

                return false;
        }

        // this function returns true whenever the user has played ahead of the cursor
        // more than a little bit, which indicates the cursor should jump.

        #user_is_ahead(tick_x) 

        {
                if (tick_x > this.#hairline_x() + 5) {
                        return true;
                }

                return false;
        }

        #next_tickindex()

        {
                // move to the next set of notes and restart the animation if it was stopped

                // setting the tickindex here will rapidly advance it to the next
                // notes the user should play (for instance, if there are several
                // bars of grayed notes in between), but it should never lead
                // to advancing beyond the last tickindex

                this.set_tickindex(this.#current_tick + 1);

                if (this.#current_tick == last_tickindex) {
                        // #finish();
                }

                // reset the sounding notes so they can be struck again
                // and the fact that they were pressed before is ignored

                // in time this looks like this:
                // 0 student strikes key
                // 1 notes_on_midi & notes_on = 1
                // 2 notes match so the sounding ones are reset:
                // 3 notes_on = 0
                // 4 any checks to see if the right keys are pressed are ignored
                // 5 student releases key(s)
                // 6 notes_on_midi = 0
                // 7 student restrikes key go back to step 1

                // without this mechanism the check at step 4 would succeed right away

                midiio.reset();

                this.#had_an_error = false;
        }

        #all_expected_notes_on()

        {
                // when playing there are no expected notes

                if (config.mode == listening) {
                        return false;
                }

                // when stopped or finished we don't do good/bad note detection

                if (this.is_stopped() || this.is_finished()) {
                        return false;
                }

                if (scrolling) {
                        return false;
                }

                var notes_sounding = midiio.sounding();

                // the condition for a note to be in error is that it isn't to be
                // played at all. Another possible condition is that the note is
                // played; released without completion of a chord. That too should count
                // as an error, but practice should continue uninterrupted. TODO

                // TODO link to piece, can we do without this?

                var notes = piece.notes_at_tickindex(this.#current_tick);

                if (notes == null) {
                        console.log("don't have any notes!");
                }

                var good_notes = this.#count_good_notes(notes_sounding, notes);

                var active_notes = this.#count_active_notes(notes);

                if (this.#count_bad_notes(notes_sounding, notes)) {
                        if (this.#had_an_error == false) {

                                // turn the noteheads for this tick red. This is a nasty
                                // little side-effect but this seems to be the best spot
                                // in the code to do it.

                                // TODO link to piece, can we do without this somehow?

                                piece.color_active_noteheads(this.#current_tick, "red");

                                // we only count one error per note or chord

                                tickstats.log(this.#current_tick, { errors: 1, good: 0, delay: 0});

                                // reset the streak on any error so that a 'rewind' or other
                                // repositioning action doesn't
                                // create a loophole to avoid resetting the streak

                                // 'punish' the mistake by stealing one of the streak built up so far
                                // this is a bit milder than the far more strict version where it 
                                // resets the streak to zero, which can make you pretty anxious when
                                // you've say built up a streak of six and then you lose it all
                                // because of a single mistake; still, it can go down pretty quickly
                                // because every error decrements the streak

                                // is this the right spot to make this change?
                                // it would seem that streak is not owned by tickstats

                                // this penalizes the student by forcing them to play it not just
                                // once more but at least twice more if they had already built up
                                // streak

                                if (this.#streak > 0) {
                                        this.#streak--;
                                }

                                // this is the evil version where a single mistake sets you back 
                                // all the way to the beginning. I suffered from pretty bad 
                                // anxiety on the last of a 7 streak run and invariably had to 
                                // start all over again when all the others went fine, there just
                                // was too much at stake so I got nervous. For now I've dropped
                                // this in favor of the milder version above. An even milder
                                // version would not reset the streak at all but then it isn't
                                // really a streak anymore but simply a recorder of the number
                                // of good plays through the exercise. Maybe we should make this
                                // changeable through the UI; on error = 'ignore, deduct, reset' 
                                // or something like that. TODO

                                // this.#streak = 0;
                        }

                        this.#had_an_error = true;

                        return false;   // have a wrong note
                }

                // The first note played marks the beginning of the practice session so we reset the time
                // this means that if someone waits a few seconds before they begin to play they do not
                // get penalized for that, the first note is always considered to be 'on time' if that
                // is where the practice starts

                if (this.#current_tick == start_tickindex) {
                        start_time = Math.floor(Date.now()/1000);
                }

                // the number of notes sounding that are expected is equal to the 
                // number of notes we are expecting in total then we are complete
                // otherwise we bail

                if (good_notes != active_notes) {
                        // too few keys pressed; ignore.
                        return false;
                }

                console.log("#state is " + this.#state + " ticks_waiting " + this.#ticks_waiting);

                // if there are no notes active it can never be a reason for a reward here. 

                if (active_notes > 0) {

                        // all the right notes are sounding
                        // that '25' is the number of ms that a note can be paused for it to count as in time
                        // TODO: ticks aren't really milliseconds, does this work as advertised?
                        // the 5000 indicates someone took a break from the keyboard for some reason, if they
                        // hit the right notes when starting we count that as a pass.

                        // modified this to use the start_tickindex instead of zero to correctly identify
                        // the beginning of the practice run.

                        if ((this.is_running() || ((this.#ticks_waiting < 50 || this.#ticks_waiting > 5000) || this.#current_tick == start_tickindex)) && (!this.#had_an_error)) {
                                // the notes were played faultless and on time, color them green as a very minor reward to the user

                                tickstats.log(this.#current_tick, { errors: 0, good: 1, delay: 0});

                                // TODO link to piece can we do without this somehow?

                                piece.color_active_noteheads(this.#current_tick, "green");

			        this.karma_update(2);
                        }
                        else {
                                // notes were good but too late

                                if (!this.#had_an_error) {

                                        console.log("too late; #state is " + this.#state + " ticks_waiting " + this.#ticks_waiting);

                                        tickstats.log(this.#current_tick, { errors: 0, good: 0, delay: 1});

				        // the notes are already lightgreen at this point in time, so no need to color them

				        this.karma_update(1);
			        }
                        }
        
                        // at this point the visual update has been done, there is still one little detail
                        // to take care of, it is possible that the user is now ahead of the point where 
                        // the cursor is, this can cause them to get confused (because the wrong notes
                        // will be sounding in the accompaniment if only one hand is practiced), and in 
                        // the case of two handed play by the user the user will eventually run off the
                        // screen on the right and will have to wait (sometimes only at the end of the
                        // piece) for the scroll to catch up. 

                        // so we will attempt to keep the scroll synchronized with the part that the user
                        // is playing and we'll re-set the position of the cursor to the position of the
                        // notes, this may cause the scroll to jump a bit to the right if this situation
                        // occurs. Smooth scrolling would be nice, but that takes time and then there 
                        // would still be a lag.

                        // if the program is set to keep up with the user when they are faster than
                        // the indicated tempo then we try to detect if this is the case and then
                        // move the scroll ahead accordingly

                        // TODO link to piece

                        if (config.keepup && this.#user_is_ahead(piece.tickindex_to_x(this.#current_tick)*2)) {
                                // and if they are we move the scroll to force it to keep up

                                this.scroll_piece_to(this.#current_tick);
                        }
                }

                // we re-enable the animation of the scroll

                if (this.is_waiting()) {
                        this.run();

                        // play any grayed out notes to accompany the user

                        // TODO link to piece

                        this.#play_grayed_notes(piece.tickindex_to_x(this.#current_tick));
                }

                this.#next_tickindex();

                return true;
        }

        // this function gets called from the mididevices script whenever the
        // state of the keyboard keys pressed has changed because of midi 
        // event(s) received.

        handler_notes_on_changed()

        {
                // after a rewind at the beginning of the piece the user can start
                // practicing by hitting the first note. 

                if ((config.mode == following || config.mode == automatic || config.mode == mode_slide || config.mode == playalong) && this.is_stopped() && this.#current_tick == start_tickindex) {
                        this.run();
                }

                // test if the user has completed the expected set of notes

                if (this.is_running() || this.is_waiting()) {
                        this.#all_expected_notes_on();
                }
        }

        // determine whether or not two intervals overlap anywhere
        // this assumes x0,x1 and s0,s1 are ordered 
        //
        // x0...x1      s0...s1 <- out left
        // x0...s0******s1...x1
        // x0...s0******x1...s1
        // s0...x0******x1...s1
        // s0...s1......x0...x1 <- out right
        // s0...x0******s1...x1

        #overlaps(x0, x1, s0, s1)

        {
                // out to the right
                if (x0 > s1) {
                        return false;
                }

                // out to the left
                if (x1 < s0) {
                        return false;
                }

                // in every other case there is overlap
                return true;
        }

        // move the piece to the left

        #animate_piece(scrolling)

        {
                // also keep track of the scaling factor
                // TODO

                // this is what drives the animation:

                if (!scrolling) {
                        this.#piece_xpos -= 2;
                }

                // Draw all canvases of which a part overlaps the screen at the right position
                // this ensures that even on slow machines or retina style displays the animation
                // is smooth. The idea here is to manipulate the absolute minimum amount of pixels.

                var x = this.#piece_xpos;

                for (var canvas=0;canvas < piece_canvases.length;canvas++) {

                        // if part of the canvas is visible then animate it
                        // this could be a little bit more optimal, that '0' could be the right hand side of the
                        // clef-timesig-keysig portion
                        // TODO

                        // unless we are completely redrawing the piece we only animate the canvases that
                        // overlap the visible part of the screen. The reason why we update all of the canvases
                        // during a redraw is to make sure that canvases that are 'off screen' get moved out of
                        // the way.

                        if (scrolling || this.#overlaps(x, x + piece_canvases[canvas].width, 0, window.innerWidth)) {

                                // console.log("animating ", canvas, " canvas ", piece_canvases[canvas].canvas , " to x ", x);

			        // document.getElementById(piece_canvases[canvas].canvas).style.top = 8 + "px";
                                document.getElementById(piece_canvases[canvas].canvas).style.left = x + "px";
                        }
                        x += piece_canvases[canvas].width;
                }
        
                if (!scrolling) {
                        if (config.mode == following || config.mode == automatic || config.mode == mode_slide) {
                                // that -1 suppresses the gray notes for a single tick
                                // so #poll_user gets a chance to pause the scrolling if the
                                // user is late.

                                this.#play_grayed_notes(Math.floor((this.#hairline_x()/2))-1);
                        }

                        // TODO link to piece

                        if (config.mode == listening && this.#on_hotspot(piece.tickindex_to_x(this.#current_tick)*2)) {
                                // we are just listening to the piece, play both left and right hand side notes

                                // TODO link to piece

                                this.#play_active_notes(piece.tickindex_to_x(this.#current_tick), lefthand + righthand);

                                this.set_tickindex(this.#current_tick+1);
                        }

                        // TODO link to piece

                        if ((this.#hairline_x() / 2) > (piece.tickindex_to_x(last_tickindex)+2)) {
        
                                // we are at the end of the piece, stop the practice,
                                // stop the interval timer to shut down the animation 
                                // and re-enable the play button

                                // subtle bug here: this causes the *last* notes not to be
                                // detected because we are stopping practice when we stop
                                // the scrolling but the very last tick still needs to
                                // be detected

                                console.log("practice stop from #animate_piece");

                                this.#finish();
                        }
                }
        }

        // rudimentary scroller.
        // takes a tickindex and moves the scroll to that point
        // also sets the tick index

        scroll_piece_to(v)

        {
                var ti = v;

                if (ti < 0) {
                        ti = 0;
                }

                // if we are in follow mode afterwards the notes that we land on will be 
                // painted lightgreen

                // TODO link to piece

                if (ti > piece.last_tickindex) {
                        ti = piece.last_tickindex;
                }

                // TODO link to piece

                var p = piece.tickindex_to_x(ti);

                // move the cursor to *just* in front of the
                // next notes to be played. 
                // ugly! where does that 740 come from?

                this.#piece_xpos = (740 - (p*2));

                this.#animate_piece(true);

                this.#_set_tickindex(ti);
        }

        // compute the position of the hairline relative to the beginning of the piece
        // this allows for precise interaction and playing back the notes at the right
        // moment in time

        #hairline_x()

        {
                //                   L   R
                //       xpos        +-|-+ cursor
                //       |-------------|-----------------|
                //            0        |

                var cursor_rect = document.getElementById("hairline").getBoundingClientRect();

                // that 10 is there because we assume noteheads are 20 pixels wide, should turn
                // this into a constant. TODO

                return (cursor_rect.left - this.#piece_xpos) - 10;
        }

        #poll_user()

        {
                // after a scroll we have some quiet time

                if (scrolling > 0) {
                        scrolling--;
                        return;
                }

                if (this.is_finished()) {
                        return;
                }

                // if we are following the user and they fell asleep then we 
                // need to pause the scrolling until the user plays
                // the right keys and wakes up again. 

                // TODO there are *six* nested ifs here, that's 64 different combinations this needs to be simplified

                if (config.mode == following || config.mode == automatic || config.mode == playalong || config.mode == mode_slide) {

                        // this accidentally also includes 'is_finished' when probably it was meant to only
                        // be is_running() || is_waiting() TODO

                        if (!this.is_stopped()) {

                                if (!this.#all_expected_notes_on()) {

                                        // we will wait for the user to catch up
                                        // if we've reached the hotspot

                                        // TODO link to piece

                                        if (this.#on_hotspot((piece.tickindex_to_x(this.#current_tick))*2)) {

                                                // maybe we should disable the animation interval timer here?

                                                // if we are going to use accompaniement MP3 tracks then
                                                // playing those back will need to be paused as well here.

                                                if (config.mode == following || config.mode == automatic || config.mode == mode_slide) {
                                                        this.wait();
                                                }

                                                // color the notes lightgreen to show that they were not hit
                                                // timely; if they were already marked as wrong then don't change

                                                if (!this.#had_an_error) {
                                                        // TODO link to piece

                                                        piece.color_active_noteheads(this.#current_tick, "lightgreen");
                                                }

                                                // in playalong mode, if you're late you won't get another chance,
                                                // we simply move on to the next note in the sequence.

                                                if (config.mode == playalong) {

                                                        // log the late note so that it shows up on the stats since
                                                        // we won't be playing it (and if we do it will cause the *next*
                                                        // note to be registered in error!)

                                                        tickstats.log(this.#current_tick, { errors: 0, good: 0, delay: 1});

                                                        this.#next_tickindex();
                                                }
                                        }
                                }
                        }
                }
        }

        // one tick of the system clock

        #one_tick()

        {
                if (this.is_finished()) {
                        return;
                }

                if ((this.is_running() || this.is_waiting()) && this.#ms_per_tick > 0) {
                        total_practiced += this.#ms_per_tick;
                }

                if (this.is_waiting()) {
                        this.#ticks_waiting++;
                        if (this.#ms_per_tick > 0) {
                                total_waiting += this.#ms_per_tick;
                        }
                }
                else {
                        this.#ticks_waiting = 0;
                }

                // we first poll the user, this takes care of the edge case where
                // the poll results in a 'pause' otherwise we will end up playing
                // any gray notes twice, once when we pause and again when we
                // unpause and that does not sound nice.

                if (scrolling) {
                        scrolling--;
                        return;
                }

                if (this.is_running()) {

                        this.#animate_piece(false);

                        this.#poll_user();

                        // attempt to reprogram the interval timer
                        // this only succeeds whenever the hairline
                        // cursor is exactly on top of new tickindex
                        // maybe this should move to #animate_piece?
                        // TODO
 
                        this.#reprogram_interval_timer();
                }
        }

        // if an interval timer is currently running 
        // then get rid of it

        #reset_interval_timer()

        {
                // if this is not the first interval timer activated then reset the previous one

                if (this.#interval_timer != null) {
                        window.clearInterval(this.#interval_timer);

                        this.#interval_timer = null;
                }
        }

        // the interval timer is free running, it drives
        // the main animation and user polling tick, and
        // because the timing can be off considerably 
        // due to browser limitations we do a lot of 
        // course correction to ensure we reach the right
        // part of the piece at the right moment in time.
        //
        // Because the user can get (considerably) ahead
        // of the tempo we have to ensure we use the 
        // actual position of the cursor rather than the
        // current_tick (because it may already point to 
        // notes much further down in the piece)

        #reprogram_interval_timer()

        {
                // first we figure out where exactly the hairline cursor is
                // in the piece 
        
                var xpos = Math.floor((this.#hairline_x()/2))-1;

                var ti = piece.xpos_to_tickindex(xpos);

                // if we are not exactly on a tick then we will not 
                // recompute how fast to scroll
                // except for when no interval timer is active, in that
                // case we make sure that it is running even if that
                // is with the wrong value

                if (this.#interval_timer != null && typeof(ti) == "undefined") {
                        return;
                }

                // we are exactly on a tick

                // we need to go to the *next* tick so the interval and delta_t to use
                // are found by taking the next tick and subtracting from the current one

	        console.log("recalculating interval timer for tick " , ti+1);

                ti = ti + 1;

                if (ti > last_tickindex) {
                        return;
                }

                var delta_x;
                var delta_t;

                if (ti == 0) {
                        // special case the run-up to the first note
                        delta_x = 50;

                        delta_t = 250;
                }
                else {
                        // all subsequent notes

                        // we *are* at ti-1, we need to go to ti

                        // the horizontal difference in pixels

                        console.log("tickindex to x ", ti-1, piece.tickindex_to_x(ti-1), ti, piece.tickindex_to_x(ti));

                        delta_x = (piece.tickindex_to_x(ti) - piece.tickindex_to_x(ti-1));

                        // the time difference in milliseconds

                        delta_t = piece.interval(ti, ti-1);
                }

                if (delta_x == 0) {
                        console.log("called #reprogram_interval_timer no delta_x so no way to reprogram the timer!");

                        if (this.#interval_timer != null) {
                                // we were called with faulty parameters, the best possible
                                // thing we can do is absolutely nothing because there already
                                // is a timer running
                                return;
                        }
                }

                this.#reset_interval_timer();

                // compute how many pixels per second we need to scroll horizontally
                // to match the tempo of the piece

                // at two pixels of scrolling per tick how many milliseconds a tick is

                this.#ms_per_tick = Math.floor(((delta_t / delta_x) / tempo_multiplier) + 0.5);	// force rounding

                console.log("#ms_per_tick ", this.#ms_per_tick, "delta_t", delta_t, "delta_x", delta_x, " multiplier ", tempo_multiplier);

                this.#interval_timer = window.setInterval(this.#bound_one_tick, this.#ms_per_tick);
        }

        // finish a practice; this is not part of the public interface (though it could be) because it
        // is the result of detecting that the end has been reached which happens in this class

        #finish()

        {
                // we can only finish a practice once

                if (this.is_finished()) {
                        return;
                }

                console.log("in practice stop");

                if ((config.mode == playalong || config.mode == following || config.mode == automatic || config.mode == mode_slide) && config.filename != null) {

                        message_user("End of the practice run; rewinding.");

                        // the practice has ended, we are out of notes.

                        console.log("end of the piece ", document.getElementById("vexsvg_00").style.left);

                        var end_time = Date.now()/1000;

                        console.log("start_time: ", start_time, " end_time: ", end_time);

                        console.log("waiting total:   ", total_waiting);
                        console.log("practiced total: ", total_practiced);

                        var should_take = total_practiced - total_waiting;

                        console.log("should take: ", should_take);

                        // use the cumulative delay of all notes and chords that were
                        // slow rather than the overall playthrough time because you don't want someone
                        // to be able to 'make up time', or scroll through a piece and upset the stats
                        // TODO

                        var speed = Math.floor(100 * (should_take / total_practiced)) * tempo_multiplier;

		        var bonus = Math.floor(((last_tickindex - start_tickindex)+1) * (speed / 100));

                        // this bit determines how the user did, and whether or not we should increment the
                        // streak or even advance the slide if we have enough good results

                        console.log("total_errors " + total_errors + " config.gradespeed " + config.gradespeed + " total_late " + total_late);

		        if (total_errors == 0 && (config.gradespeed != 1 || total_late == 0)) {
		        	// flawless
			        this.#streak++;

			        message_user("No errors, " + bonus + " karma bonus!");

                                // if the mode is 'slide' and the user has shown that they really
                                // know this part very well then we will move one or more bars
                                // forward in time to let them practice a new part of the piece

                                if (this.#streak >= slide_grade && config.mode == mode_slide) {
				        // reset the streak and the streak display

				        this.#streak = 0;

                                        // force display without changing karma

                                        this.karma_update(0);

				        // and advance the slide

                                        planners.advance(piece);
                                }
		        }
		        else {
			        // reset the streak back to zero because there was
			        // some kind of error
			
                                if (total_errors == 0) {
                                        // when grading for speed we may choose to ignore this performance even if it
                                        // was without errors because it was too slow; the user will get the bonus
                                        // for the fault free practice

                                        if (config.gradespeed == 1 && total_late > 0) {
                                                message_user("Too slow, but no errors, " + bonus + " karma bonus!");
                                        }
                                }
                                else {
                                        // streak has already been decremented elsewhere

			                // this.#streak = 0;

                                        bonus = 0;
                                }
		        }

                        // update bonus and display streak updates

                        this.karma_update(bonus);

                        console.log(config.filename, start_tickindex, last_tickindex, total_errors, total_good, total_late, Math.floor(total_practiced/100)/10, speed);

                        // if the user was practicing then save the practice session statistics

                        practicelog.save(config.filename, new Date(), start_tickindex, last_tickindex, total_errors, total_good, total_late, Math.floor(total_practiced/100)/10, speed, config.hand);

                        practicelog.load(config.filename, start_tickindex, last_tickindex, config.hand);

                        // make sure the stats are saved 

                        piece_save();

                        tickstats.update_scores(piece);

                        window.setTimeout(function () {
			        if (config.mode == automatic && total_errors == 0) {
                                        // if there were no errors then compute a new practice range, 
                                        // otherwise repeat the exercise until the user gets it right
				        planners.autopilot_practice_range(piece);
			        }
			        else {
                        	        rewind();
			        }
                        }, 2000);                
                }

                this.#reset_interval_timer();

                this.#state = finished;
        }

        // move to a new tickindex, effectively moving to a new set of notes
        // because the tempo can change from one tickindex to the next and
        // because ticks are variable width we need to reprogram the interval
        // timer here to ensure the scroll speed is correct

        #_set_tickindex(ti)

        {
                if (ti != this.#current_tick) {

                        this.#current_tick = ti;

                        // reset the 'had an error' flag, presumably we are going to 
                        // move to a new set of notes now so there is no error in them
                        // so far

                        this.#had_an_error = false;
                }

                // we are on the move again; regardless of how we got here
                // we enable the animation to continue play unless we are at
                // the end of the piece.

                if (this.is_waiting()) {
                        this.run();
                }
        }

        // to avoid creating a loop where a change to the scrollbar
        // causes the scrollbar to be animated we call a hidden
        // version of set_tickindex here and use that when we
        // manipulate the scrollbar. The hidden one does not update
        // the scrollbar position.

        set_tickindex(ti)

        {
                this.#_set_tickindex(ti);

                animate_scrollbar(this.#current_tick);
        }

        // switch to 'animated' mode, this is where the realtime class gets activated to take over
        // the display from the user until the end of the practice or it is stopped

        run()

        {
                this.#state = running;

                this.#ticks_waiting = 0;

                if (this.#current_tick < last_tickindex) {

                        // if we start playing right on top of grayed
                        // out notes make them sound

                        if (config.mode == following || config.mode == automatic || config.mode == mode_slide) {
                                this.#play_grayed_notes(Math.floor((realtime.#hairline_x()/2)-1));
                        }
                }

                this.#reprogram_interval_timer();
        }

        stop()

        {
                this.#state = stopped;

                // make sure the stats are saved 
                // this isn't really an appropriate place to make a call back into index.js code
                // better would be call to piece. or to have this done in the housekeeping function

                piece_save();

                midiio.notes_off();
        }

        wait()

        {
                this.#state = waiting;

                this.#ticks_waiting = 0;
        }

        get_current_tick()

        {
                return this.#current_tick;
        }
}

realtime = new Realtime();



