//
// Everything related to generic midi device input and output
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 
// 

// This file is now ready to be turned into a class; there are still
// a couple of details that need taking care of, for instance, the
// callback function to the main program whenever a midi note is
// set to on or off is still hardcoded and should be passed into 
// the constructor, and there are two arrays, one for the notes
// that are 'on' to be able to show the midi note cursors and 
// another that is essentially the same thing but that can be
// reset from the other side in order to allow for a restrike
// which seems to me like it could be done more elegantly. 
// Ideally the notecursors should get their own entity which
// this class then builds on, leaving only one array for the
// interface to the main program. Maybe that should be done
// first before converting this file into a class in its own right
// to make it easier to see the distinction between the note
// cursors and the notes themselves.

// these two should probably move to a 'settings.js' file

// minimum velocity to register a note as played
// disklavier pianos apparently are hyper sensitive and when the volume
// is turned down they need to be played quite hard to register a keypress,
// which is the opposite of what you'd want.
//
// This should be made user configurable TODO

// deal with MIDI devices, we need at least an input device

// check to see if more functions can be made private TODO

class Midiio {

        #synth;
        #input_devices;
        #output_devices;
        #cursors;               // the state of the cursors 1:1 tied to the midi events received
        #notes_on;              // the state of the notes as seen by the rest of the program
                                // reset whenever a set of good notes is seen so they need to
                                // be struck again before the program will evaluate them

        constructor()

        {
                if (navigator.requestMIDIAccess) {
                        var bound_on_midi_success = this.#on_midi_success.bind(this);

                        navigator.requestMIDIAccess().then(bound_on_midi_success, this.#on_midi_failure);
                } 
                else {
                        // redirect here to a page about WebMiDI

                        window.location.href = "firefox.html";
                }

                this.#synth = null;          // The Tone.js synthesizer used for the builtin audio
                this.#input_devices = [];
                this.#output_devices = [];
                this.#cursors = [];
                this.#notes_on = [];
        }

        // convert the notes_on bitmap into a compact array with just the sounding notes

        sounding()

        {
                var notes_sounding = [];
                var n = 0;

                for (var i=0;i<128;i++) {
                        if (this.#notes_on[i] > 0) {
                                notes_sounding[n++] = i;
                        }
                }

                return notes_sounding;
        }

        // reset the notes_on bitmap (this effectively eats up the notes until they are re-struck)

        reset()

        {
                for (var i=0;i<128;i++) {
                        this.#notes_on[i] = 0;
                }
        }

        #show(inputs, outputs) 

        {
                selectIn.innerHTML = this.#input_devices.map(device => `<option>${device.name}</option>`).join('');
                selectOut.innerHTML = this.#output_devices.map(device => `<option>${device.name}</option>`).join('');
        }

        // this is called once at the beginning of a run to determine which midi devices
        // are available. If a device is hotplugged then the page will have to be refreshed
        // to make the device show up in the list of devices. Once the list of devices has
        // been built up it is used to populate the drop downs, show the 
        // right devices as selected and to send a note_off command to shut
        // down any possible hanging notes from a previous run.

        #on_midi_success(midiAccess) 

        {
                this.#input_devices = [];

                // MIDI devices that can send you data.

                const inputs = midiAccess.inputs.values();

                for (let input = inputs.next(); input && !input.done; input = inputs.next()) {
                        this.#input_devices.push(input.value);
                }

                // MIDI devices that you can send data to.

                // first we add the builtin tone generator

                this.#output_devices = [{ name: 'Builtin Audio' }];

                // then we add all the midi output devices that we can find.

                const outputs = midiAccess.outputs.values();

                for (let output = outputs.next(); output && !output.done; output = outputs.next()) {
                        this.#output_devices.push(output.value);
                }

                // put them all in the drop down

                this.#show();

                this.set_input(config.midiin);
                this.show_selected_output(config.midiout);

                this.notes_off();        
        }

        show_selected_input(selected)

        {
                document.getElementById("selectIn").selectedIndex = selected;
        }

        // what happens here when show_selected_input is called
        // from input_change? Does that possibly generate another event?

        set_input(selected)

        {
                config.midiin = selected;

                var i = 0;
                for (var input of this.#input_devices) {
                        if (i == selected) {
                                var bound_get_message = this.#get_message.bind(this);

                                input.onmidimessage = bound_get_message;
                        }
                        else {
                                input.onmidimessage = null;
                        }
                        i++;
                }

                this.show_selected_input(selected);
        }

        show_selected_output(selected) 

        {
                document.getElementById("selectOut").selectedIndex = selected;
        }

        // callback called whenever the drop down selected element is modified

        input_change() 

        {
                var selected = document.getElementById("selectIn").selectedIndex;

                this.set_input(selected);
        }

        output_change()

        {
                var selected = document.getElementById("selectOut").selectedIndex;

                config.midiout = selected;

                if (selected == 0) {
                        // start up Tone.js
                        this.start_tone_js();
                        console.log("tonejs on");
                }
                else {
                        // disable Tone.js
                        console.log("tonejs off");
                        this.#synth = null;
                }
        }

        #on_midi_failure() 

        {
                document.querySelector('.note-info').textContent = 'Error: Could not access MIDI devices. Connect a device and refresh to try again.';
        }

        #send_message(message, pitch, velocity) 

        {
                if (selectOut.selectedIndex > 0) {
                        this.#output_devices[selectOut.selectedIndex].send([message, pitch, velocity]);
                }
        }

        // for some reason I can't get the command 'allnotesoff' to be working
        // directly so I've hacked it by sending 'off' commands for all notes
        // that might be active. This is obviously wasteful on the midi channel
        // and on older serial devices will probably lead to congestion.

        // possible optimization: keep track of which notes have had a note-on
        // command sent without a corresponding note-off and send note-offs
        // for those (and reset the bits that record whether they are sounding
        // or not)

        all_notes_off()

        {
                if (selectOut.selectedIndex == 0) {
                        // TODO


                }
                else {
                        const device = this.#output_devices[selectOut.selectedIndex];
                      
                        var i;

                        for (i=21;i<109;i++) {
                                device.send( [0x90, i, 0] );
                        }
                }
        }

        // this uses the internal state variables of the selector to figure out
        // the device ID, that seems to be a bit silly TODO

        play_note(pitch, duration, velocity)

        {
                if (selectOut.selectedIndex == 0) {
                        // device index 0 is the built in tone generator

                        var now = Tone.now();
                        var note = Tone.Frequency(pitch, "midi").toNote();
                        // for some reason a negative value here removes some of the latency that
                        // Tone.js introduces. That probably isn't the best solution
                        // TODO
                
                        // avoid underflowing the attack moment 

                        let attack = now - 0.100;

                        if (attack < 0) {
                                attack = 0;
                        }

                        this.#synth.triggerAttack(note, attack);
                        this.#synth.triggerRelease([note], now + duration / 1000);
                }
                else {
                        // isn't there a cleaner way to get to this device?

                        const device = this.#output_devices[selectOut.selectedIndex];

                        // note 'on'
                        device.send( [0x90, pitch, velocity]);

                        // console.log("pitch ", pitch, "duration", duration);

                        if (duration != -1) {   // check for 'infinite sustain'; these are turned off explicitly
                                // note 'off' after duration delay

                                device.send( [0x90, pitch, 0], window.performance.now() + duration );
                        }
                }
        }

        // this is a public function
        // sends midi off messages for all notes and resets the cursors and the sounding arrays

        notes_off()

        {
                for (var i=0;i<128;i++) {
                        if (this.#notes_on[i] || this.#cursors[i]) {
                                this.#notes_on[i] = 0;
                                this.#cursors[i] = 0;

                                this.play_note(i, 0, 0);
                        }
                }
        }

        // Receive a MIDI messages from the attached MIDI input device, 
        // this keeps track of which keys are down right now on the keyboard
        // and will call the matcher to see if those keys match the expected
        // keys in 'following' mode. 

        #get_message(message) {
                var command = message.data[0];
                var note = message.data[1];
                var velocity = message.data[2];

                if (current_tab == tabsoundguesser) {
                        // we're in sound guessing mode, pass the message
                        sg_message(command, note, velocity);

                        return;
                }

                // an experimental chorus feature; it works like the couplers
                // on a pipe organ where you get to add in other manuals or to 
                // play notes above or below they key you press

                var chorus = false;

                switch (command) {
                        case 144: // noteOn

                                // dirty hack to use A0 on an 88 key keyboard
                                // as an 'abort' button to stop the current run
                                // and not to save the performance

                                if (note == 21) {
                                        piece_retrieve(config.filename);

                                        return;
                                }


                                // various couplers like in an organ

                                if (note >= 60) {
                                        // treble, couple 'up'
                                        if (config.coupleplusplus) {
                                                if (note+24 < 127) {
                                                        console.log("writing for plusplus\n");

                                                        const device = this.#output_devices[selectOut.selectedIndex];

                                                        device.send( [0x90, note+24, velocity]);
                                                }
                                        }

                                        if (config.coupleplus) {
                                                if (note+12 < 127) {
                                                        const device = this.#output_devices[selectOut.selectedIndex];

                                                        device.send( [0x90, note+12, velocity]);
                                                }
                                        } 
                                }
                                else {
                                        // bass, couple 'down'
                                        if (config.coupleminus) {
                                                if (note-12 >= 0) {
                                                        const device = this.#output_devices[selectOut.selectedIndex];
                                                
                                                        device.send( [0x90, note-12, velocity]);
                                                }
                                        }
                                        
                                        if (config.coupleminusminus) {
                                                if (note-24 >= 0) {
                                                        const device = this.#output_devices[selectOut.selectedIndex];
                                                
                                                        device.send( [0x90, note-24, velocity]);
                                                }
                                        }
                                }

                                // some pianos are super sensitive and will already send note
                                // on messages when the keys are just brushed and nothing is audible
                                // maybe this should become configurable by the user so they can
                                // adapt this value to best suit their instrument

                                if (velocity > config.minvelocity) {
                                        this.#notes_on[note] = 1;
                                        this.#cursors[note] = 1;

                                        // another magic key, A#0 / Bb0 to reveal the main menu
                                        // as long as it isn't ready, this serves as a crude
                                        // feature flag that can be switched on from the keyboard

                                        if (note == 22) {
                                                $( '#mainTab' ).show();
                                                console.log('revealing mainTab');
                                        }

                                        if (config.echoinout && this.#synth != null) {
                                                // this will need some more subtlety, for instance
                                                // right now you can't hold a key down, release it
                                                // and have the expected result
                                                // TODO: treat internal synth through midi messages

                                                this.play_note(note, 0.5, velocity);
                                        }
                                }
                                else {
                                        this.#notes_on[note] = 0;
                                        this.#cursors[note] = 0;
                                }

                                break;
                        case 128:       // note Off

                                // various couplers like in an organ

                                if (note >= 60) {
                                        // treble, couple 'up'
                                        if (config.coupleplusplus) {
                                                if (note+24 < 127) {
                                                        const device = this.#output_devices[selectOut.selectedIndex];

                                                        device.send( [0x90, note+24, 0]);
                                                }
                                        }

                                        if (config.coupleplus) {
                                                if (note+12 < 127) {
                                                        const device = this.#output_devices[selectOut.selectedIndex];

                                                        device.send( [0x90, note+12, 0]);
                                                }
                                        }
                                }
                                else {
                                        // bass, couple 'down'
                                        if (config.coupleminus) {
                                                if (note-12 >= 0) {
                                                        const device = this.#output_devices[selectOut.selectedIndex];

                                                        device.send( [0x90, note-12, 0]);
                                                }
                                        }

                                        if (config.coupleminusminus) {
                                                if (note-24 >= 0) {
                                                        const device = this.#output_devices[selectOut.selectedIndex];

                                                        device.send( [0x90, note-24, 0]);
                                                }
                                        }
                                }

                                this.#notes_on[note] = 0;
                                this.#cursors[note] = 0;
                                break;
                        case 254:
                        case 248:
                                return;
                        default:
                                return;
                                break;
                }

                // send the audio back out to the output device so you can hear what is being played

                if (config.echoinout) {
                        if (this.#synth == null) {
                                this.#send_message(command, note, velocity);
                        }
                }

                notecursors.show_note_cursors(this.#cursors);

                // callback to the main program; this should be set during initialization
                // so the code is more generic.

                realtime.handler_notes_on_changed();
        }

        // this is a public function

        start_tone_js()

        {
                if (this.#synth != null) {
                        return;
                }

                if (config.midiout != 0) {
                        return;
                }

                // create a sampler synth, reusing the salamander piano
                // files from Tone js. in the future will be good to find
                // the full set and host ourselves.
                this.#synth = new Tone.Sampler({
                        urls: {
                            "A0": "A0.mp3",
                            "A1": "A1.mp3",
                            "A2": "A2.mp3",
                            "A3": "A3.mp3",
                            "A4": "A4.mp3",
                            "A5": "A5.mp3",
                            "A6": "A6.mp3",
                            "A7": "A7.mp3",
                            "C1": "C1.mp3",
                            "C2": "C2.mp3",
                            "C3": "C3.mp3",
                            "C4": "C4.mp3",
                            "C5": "C5.mp3",
                            "C6": "C6.mp3",
                            "C7": "C7.mp3",
                            "C8": "C8.mp3",
                            "D#1": "Ds1.mp3",
                            "D#2": "Ds2.mp3",
                            "D#3": "Ds3.mp3",
                            "D#4": "Ds4.mp3",
                            "D#5": "Ds5.mp3",
                            "D#6": "Ds6.mp3",
                            "D#7": "Ds7.mp3",
                            "F#1": "Fs1.mp3",
                            "F#2": "Fs2.mp3",
                            "F#3": "Fs3.mp3",
                            "F#4": "Fs4.mp3",
                            "F#5": "Fs5.mp3",
                            "F#6": "Fs6.mp3",
                            "F#7": "Fs7.mp3",                
                        },
                        release: 1,
                        baseUrl:  "https://tonejs.github.io/audio/salamander/"
                }).toDestination();
                // the lookAhead defaults to 0.1s in the future, whereas we want
                // our audio events to play in realtime.
                this.#synth.context.lookAhead = 0;
        }

        // this is a public function (called from the UI)

        echoinout_change(e)

        {
                if ($("#echoinout").is(":checked")) {
                        config.echoinout = 1;
                }
                else {
                        config.echoinout = 0;
                }
        }
}

var midiio = new Midiio();


