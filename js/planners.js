//
// Planners are responsible for implementing the various practice
// modes such as slide/auto etc. Each of these creates a dynamic
// practice session and has a method of advancing the student 
// through the piece when the practice session has been completed.
//
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 
// 

// The autopilot, this guides the user through practicing a new piece until they have mastered it.
// Ideally this would be completely stateless and re-starting it from any point in a practice
// would have the exact same effect.

class Planners {
	constructor()

	{
                this.autopilot_start_offset = 0;
                this.autopilot_end_offset = 0;
	}

        // advance gets called every time the streak counter
        // registers the required number of error free passes through the
        // segment. The segment is then moved forward in the piece if
        // possible, and if not possible because the end is reached
        // we rewind to the beginning and increase the segment length
        // to up the difficulty factor

        // this is a public function (called from index.js)

        // this function should receive all of its input as parameters
        // and should return the new position

        advance(piece)

        {
                // figure out how long the segment was

                var dti = (last_tickindex - start_tickindex);

                var new_start_bar;

                if (last_tickindex >= piece.last_tickindex) {
                        // we have finished the piece at this setting
                        // back to the beginning but with a larger section

                        new_start_bar = 0;

                        dti = (2 * (dti+1)) - 1;
                }
                else {
                        // the piece has not finished yet, figure out which bar
                        // to start at next

                        var old_start_bar = piece.tickindex_to_bar(start_tickindex);

                        new_start_bar = piece.tickindex_to_bar(last_tickindex);

                        if (new_start_bar == old_start_bar) {
                                new_start_bar++;
                        }

                        if (new_start_bar >= piece.bars.length) {
                                new_start_bar = piece.bars.length-1;
                        }
                }

                // move both start and end to their new positions

                start_tickindex = piece.bars[new_start_bar].tickindices[0];

                last_tickindex = start_tickindex + dti;

                // clamp the segment to the piece to ensure we are 
                // always in between the 0...last_tickindex range

                if (last_tickindex > piece.last_tickindex) {
                        last_tickindex = piece.last_tickindex;

                        start_tickindex = piece.last_tickindex - dti;
                }

                if (start_tickindex < 0) {
                        start_tickindex = 0;
                }
        }

        // the autopilot_practice_range function is called once per 
        // practice run (usually at the end, but on a mode switch
        // once at the beginning). 

        // it will determine the appropriate range to practice next
        // based on where the student has scored worst in the piece

        // public function; called from realtime.js

        autopilot_practice_range(piece)

        {
	        console.log("autopilot_practice_range!");

	        if (piece == null) {
		        console.log("no piece!");
		        return;
	        }

	        console.log("piece.bars.length", piece.bars.length);

	        // if there are no bars then there is no work.

	        if (piece.bars.length == 0) {
		        console.log("no bars");

		        return;
	        }

	        // computes the worst_bar variable as well as the progress
                // probably tickstats.update_scores should return a tuple with worst, average etc
                // that way we don't have to reach into the guts of it

                tickstats.update_scores(piece);

	        // figure out if we have enough data to work with, if not we will
	        // command a playthrough

	        var start = tickstats = tickstats.no_coverage_from(piece);

	        if (start >= 0) {
		        console.log("not enough data yet, starting playthrough at ", start);
		        start_tickindex = start;
		        last_tickindex = piece.last_tickindex;
	        }
	        else {
		        // we have enough data to determine where the student is still weak

		        var start_bar = tickstats.worst_bar;
		        var end_bar = tickstats.worst_bar;

		        var start_offset = 0;
		        var end_offset = 0;

                        // depending on what the worst bar scores like we will now
                        // do 1, 3, 5, 7 or 9 bars of the piece (the closer we get to
                        // perfection the longer the segments you get to practice)

                        console.log("worst bar score is ", tickstats.bar_scores[0].score);

                        if (tickstats.bar_scores[0].score > 0.5) {
                                console.log("worst > 0.5");

                                start_offset--;
                        }

                        if (tickstats.bar_scores[0].score > 0.85) {

                                console.log("worst > 0.85");

                                end_offset++;
                        }

                        if (tickstats.bar_scores[0].score > 0.9) {
                                console.log("worst > 0.9");
                                start_offset--;
                        }

                        if (tickstats.bar_scores[0].score > 0.95) {
                                console.log("worst > 0.95");
                                end_offset++;
                        }

                        if (tickstats.bar_scores[0].score > 0.98) {
                                console.log("worst > 0.98");
                                start_offset--;
                                end_offset++;
                        }

		        // ensure that we don't fall back to smaller sections once the
		        // player has reached a level where they can start practicing longer ones
		        // otherwise you end up flip-flopping over and over again at that border
                        // which does not build confidence

                        if (start_offset < this.autopilot_start_offset) {
                                this.autopilot_start_offset = start_offset;
                        }

                        if (end_offset > this.autopilot_end_offset) {
                                this.autopilot_end_offset = end_offset;
                        }

                        start_bar = start_bar + this.autopilot_start_offset; // note that the offset itself will be negative so we ADD it!
                        end_bar = end_bar + this.autopilot_end_offset;

                        if (start_bar < 0) {
                                start_bar = 0;
                        }

                        if (end_bar >= piece.bars.length) {
                                end_bar = piece.bars.length-1;
                        }

                        console.log("start_bar: ", start_bar, "end_bar: ", end_bar);

                        if (start_bar > end_bar) {
                                debugger;
                        }

                        start_tickindex = piece.bars[start_bar].tickindices[0];

                        console.log(piece.bars[start_bar].tickindices);

                        last_tickindex  = piece.bars[end_bar].tickindices[
                                          piece.bars[end_bar].tickindices.length-1
                        ];

                        // protect against start bars without content

                        console.log("start_tickindex: ", start_tickindex);
                        console.log("last_tickindex: ", last_tickindex);

                        if (typeof(start_tickindex) == 'undefined') {
                                start_tickindex = 0;
                        }

                        // JM: I have disabled this test because it once triggered and
                        // analyzing the situation that caused it my conclusion is that
                        // this is not a bug but can actually happen: if the last bar
                        // has a single note that has been played wrong then the autopilot
                        // may well zoom in on that single note to get the student to
                        // play it right. 

                        // if (start_tickindex >= last_tickindex) {

                                // debugger;
                        // }

                        if (typeof(last_tickindex) == 'undefined') {
                                debugger;
                        }
	        }

                rewind();

                console.log("autopilot done, new autopilot offsets ", this.autopilot_start_offset, this.autopilot_end_offset);
        }

        // The init function is called once upon a mode change
        // it can be used to re-initialize the autopilot to a 
        // known 'safe' state upon re-engagement.

        // public function; called from index.js

        autopilot_init(piece)

        {
	        console.log("resetting autopilot offsets");

	        this.autopilot_start_offset = 0;	// we start off at one bar
	        this.autopilot_end_offset = 0;

                tickstats.update_scores(piece);

                if (config.mode == automatic) {
	                this.autopilot_practice_range(piece);
                }
        }

}

let planners = new Planners();


