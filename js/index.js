//
// Attempt at hacking together a music trainer in JavaScript
// (C) Jacques Mattheij 2020; 2021; 2022; 2023; jacques@modularcompany.com
// All rights reserved, 
// 
// This project builds on WebMIDI which is not supported in all browsers
// so for now you'll have to use Chrome (sorry.). Another important 
// component is VexFlow, a musical score rendering library.
//
// The idea here is to first make a web browser based replacement for
// PianoBooster, then to turn it into a full fledged course to learn
// how to play the piano, with automatic lesson plans and the ability
// to upload your own MIDI files of repertoire that you want to learn how
// to play. I want it to be web based so progress and analysis can eventually 
// be done server side and data can be persisted without the user having
// to worry about that, another important reason is to be as much 
// multi platform as possible. For now the program stores all data 
// locally, this removes any kind of security risks and also ensures
// that if someone wants to use the software offline or in an archived
// version in the future that it will work just fine.
//
// Note that I absolutely loathe javascript, it is a terrible language
// but in the interest of portability and transparency as well as to 
// reduce the number of dependencies in building this I will use straight
// js for the whole project. This also opens up the project to far
// more people as potential contributors.
//
// One reason in particular that I seriously dislike javascript is
// because computer programming would be hard enough if it weren't
// for asynchronous code to make our lives even harder, and javascript
// is a never ending hodgepodge of callbacks, promises and other
// nonsense. It allows for some interesting effects at the expense
// of *very easily* losing track of what is going on with your code
// which even for experienced programmers (40 years and counting)
// can be a source of distraction and frustration. I've written
// an operating system and some pretty complex interrupt driven
// device drivers, so it's not as if real time or async stuff 
// scares me. Even so, javascript can turn a simple piece of software
// into something insanely hard to debug. Even mundane operations
// such as persisting a record into a database are done asynchronously
// which really does not help if you are trying to get some work done.
// It's Callback Hell.
//
// This is not the most elegant code when it comes to decoupling 
// presentation and logic, one reason is that there are a lot of
// time constraints, another that I don't see many reasons why the
// presentation layer would ever change, it either works or it 
// doesn't and so far it seems it will work. 
//
// For now I've abandoned the idea of importing music XML, instead 
// we will import MIDI directly from binary files; which presents
// its own set of complexities and given the lossy nature of midi
// recovery of certain traits will be required, and may not always
// be possible. This is annoying but given the abundance of MIDI
// files it does not look as though there is another contender, nothing
// else comes close from a repertoire perspective. I'm toying with
// the idea of using MIDI text fields to annotate the piece so that
// you can add hints on how to render a particular bit in a more
// visually appealing way (for instance: mordants and trills).
// This isn't simple: a note missed in a trill would result in the
// trill being faulty, but that particular note does not have any
// representation in the midi file!
//
// Another idea is to use flashcards and spaced repetition to train
// sightreading skills; see 'CHANGELOG' for a whole pile more
// of what could go into future versions.
//
// Finally, an ear training module could play notes and chords
// which the user would then have to duplicate. 
//
// Including jquery for one call is also overkill, probably should 
// get rid of that and code up a small replacement function to erase
// the score? tree in the SVG box
//
// One tricky bit in music notation is that due to 'sharps' and 'flats'
// one pitch value (say 61) can be more than one note in notation, in 
// this instance a C# or a Db 
//
// About those ticks...
// 
// 'tick' has to be the most overloaded term in this project. Midi files
// use 'ticks' to indicate beats of a clock that are on a much finer
// grained time scale than a note or a bar. A typical quarter note in
// 'midi time' can be anywhere from 10 to 480 ticks from what I've seen
// in practice. Depending on the tempo, so this is actually a variable
// amount of time.
//
// Another way in which the word tick is used in this program is in the
// context of VexFlow, the music rendering library. VexFlow calls
// everything that is a part of a stave a 'tickable', which means that
// some measure of time is passing while you're looking at it. This is
// opposed to decoration such as clefs, bar lines and so on. But rests 
// and anything related to notes and the notes themselves are tickables.
//
// To avoid a similar situation around 'score' in the sense of the 
// musical score and the score for a bar or tick the word 'piece' is used
// to denote the musical entity, and multiple pieces are called repertoire.
//
// Within repertoire there should be another grouping called 'songbooks'
// along the lines of what pianobooster offers.
//
// Fortunately - so far - there is no link with blood sucking insects 
// called 'ticks' or this program would be really impossible to understand. 
// But give it some time ;)
//
// In this file you will find:
// all of the code related to user interaction, the UI responses and updates
// and anything related to the real-time part of the program.

// At 3Kloc it is on the heavy side, and the async nature of both the
// MIDI interface and the persistence layer make for confusing reading
// at times so tread carefully. Testing this code is very hard, it is
// as far as I can see next to impossible to mock the typical user 
// interaction and the various pieces are tied together much stronger
// than I would like them to be but I don't see an easy way to split
// them up. This is a continuous area of focus though.

// The drivers for any kind of activity by the program are:
// - user interaction through the web browser
// - the timer that drives the movement of the graphical representation
//   of the piece scrolling left to right as stored in a number of canvases 
// - the reception of midi messages
//
// Each of these has a handler. 

console.log("version 0.12");

// Move to the naked domain if we're called with www.*, this
// to ensure that we don't accidentally create a new database if a
// user specifies the www. subdomain, which would likely confuse
// the user because all of their previous work would appear to be 
// gone.

if (window.location.hostname == 'www.pianojacq.com') {
        // redirect to naked domain
        window.location.replace("https://pianojacq.com");
}

// first order of battle: connect to the db

db_connect();

// get rid of horizontal scrollbar for the whole document
// can this be moved to the style sheet?

document.documentElement.style.overflowX = 'hidden';

function mode_change()

{
	var old_mode = config.mode;

        config.mode = $("#mode").val();

        // we have several grades of 'slide' so end up with the
        // mode set to 3 and slide_grade set to the fraction times 10.
	// nasty, comparing for equality on floats!

        if (config.mode == 3.0) {
                slide_grade = 1;
        }
        if (config.mode == 3.1) {
                slide_grade = 1;
        }
        if (config.mode == 3.3) {
                slide_grade = 3;
        }
        if (config.mode == 3.7) {
                slide_grade = 7;
        }
        
        config.mode = Math.floor(config.mode);

	// if we are backing out of automatic mode then rest the area to practice to the whole piece
	// this is what you usually want to do; it does stop you from repeat practicing a passage that
	// auto found for you so maybe this should be configurable?

	if (config.mode == following && (old_mode == automatic || old_mode == mode_slide)) {
		start_tickindex = 0;

                last_tickindex  = piece.last_tickindex;

		rewind();
	}

        if (config.mode == hardchords) {
                var midi = generate_hard_chords_lesson(piece);

                piece_add("hardchords", midi);

                config.mode = following;
        }

        realtime.stop();

	planners.autopilot_init(piece);

        realtime.run();
}

// difficulty factor change

function diff_change()

{
        config.diff = $("#diff").val();

        redraw();
}

function piece_save()

{
        piece_persist(config.filename, piece_midi, tickstats.get_per_tick_stats(), start_tickindex, last_tickindex);
}

// called async from config.js when the config is loaded 

function config_loaded()

{
        // ideally, here we should allow the overriding of configuration 
        // variables through the query parameters as well so that a 
        // lesson can start in a particular mode, at a particular spot etc
	// TODO

	// now reflect all of the configuration elements read in the UI

	realtime.karma_update(0);

        $("#selectKeys").val((config.kbd_midi_high-config.kbd_midi_low)+1); 

        // ensure that the UI state is in sync with the retrieved settings

        $("#hand").val(config.hand);

        // force the config mode to something that will be recognized

        // ugly hack to restore to 'slide' mode, which uses the fraction
        // as the way to indicate how many flawless streaks there should
        // be before advancing

        // hm, somehow the fraction made it into the database
        // this really should go to config.js

        config.mode = Math.floor(config.mode);

        if (config.mode == mode_slide) {
                $("#mode").val(3.3);
        }
        else {
                $("#mode").val(config.mode);
        }

        $("#diff").val(config.diff);

        // this should all move to mididevices

        midiio.show_selected_output(config.midiout);
        midiio.set_input(config.midiin);        

        $("#echoinout").prop('checked', config.echoinout ? true : false);

        // up to here

        $("#keepup").prop('checked', config.keepup ? true : false);

        $("#gradespeed").prop('checked', config.gradespeed ? true : false);

        $("#notenames").prop('checked', config.notenames ? true : false);

        $("#notecursors").prop('checked', config.notecursors ? true : false);

        // console.log("done with retrieving config");

	// if an exercise was specified as a query parameter use that one
	// otherwise retrieve the previously used one from the db

	if (!load_query_parameters()) {
        	piece_retrieve(config.filename);
	}
}

var redraw_filename = '';
var redraw_hand = -1;
var redraw_diff = -1;
var redraw_klow = -1;
var redraw_khigh = -1;
var redraw_start = -1;
var redraw_last = -1;
var redraw_force = false;

// figure out if enough has changed since the last time we were here that we need to redraw the piece

function must_redraw(start_tickindex, last_tickindex)

{
	var r = false;

        if (redraw_force) {
                r = true;
                redraw_force = false;
        }

	if (config.filename != redraw_filename) {
		r = true;
	}
	if (config.hand != redraw_hand) {
		r = true;
	}
        if (config.diff != redraw_diff) {
                r = true;
        }
	if (config.kbd_midi_low != redraw_klow) {
		r = true;
	}
	if (config.kbd_midi_high != redraw_khigh) {
		r = true;
	}
	if (start_tickindex != redraw_start) {
		r = true;
	}
	if (last_tickindex != redraw_last) {
		r = true;
	}

	redraw_filename = config.filename;
	redraw_hand = config.hand;
        redraw_diff = config.diff;
	redraw_klow = config.kbd_midi_low;
	redraw_khigh = config.kbd_midi_high;
	redraw_start = start_tickindex;
	redraw_last = last_tickindex;

	return r;
}

function redraw()

{
	if (piece == null || must_redraw(start_tickindex, last_tickindex)) {
        	piece = new Piece(piece_midi, piece_canvases, canvas_width, config.diff);
	
        	// timesig, keysig and scale should be supplied by piece, not some global
        	// TODO

        	draw_clefs_keysig_timesig(keysig, scale, timesig);

        	draw_filler();
	}
        // now we know how many tickables there really are in the
        // piece so we can set the range of the sliders

        // but only if the sliders have initialized (otherwise it doesn't
        // matter anyway...)

        if($("#scroller").slider("instance")) {
                $("#scroller").slider("option", "max", piece.last_tickindex);
                $("#slider-range").slider("option", "max", piece.last_tickindex);
		$("#slider-range").slider("values", [start_tickindex, last_tickindex]);

        }

	tickstats.update_scores(piece);

        // draw all ticks that had errors in the last 10 plays in red

        for (var i=start_tickindex;i<=last_tickindex;i++) {
                if (tickstats.tick_had_errors(i)) {
                        piece.color_active_noteheads(i, "red");
                }
        }
}

// maybe this should go to realtime?

function stats_counters_reset()

{
        start_time = Math.floor(Date.now()/1000);

        total_errors = 0;
        total_good   = 0;
        total_late   = 0;

        total_waiting = 0;

        total_practiced = 0;
}

// rewind is a border case, it affects a lot of the stuff from realtime.js
// but it isn't *actually* doing anything realtime so that is why it is 
// on this side of the line. But there are good arguments to be made to
// move it to realtime.js

var rewinding = false;

function rewind()

{
        if (rewinding) {
                return;
        }

        rewinding = true;

        practicelog.start();

	// this redraw on every rewind is much too time consuming.
        // either the redraws themselves should become much faster
        // or we should use them more sparingly (it's a nice and 
        // quick wy to get the display back in sync though)
	// TODO

        redraw();

        // move the animation to the leftmost point.

        realtime.set_tickindex(-1);

        // a rewind is a good moment to save the piece and the associated statistics
        // we probably should do this at the end of a piece too, in case rewind is
        // never triggered

        // we probably should save the lesson plan and first and last bar as well so
        // we can pick up again where we left off later.

        piece_save();

        stats_counters_reset();

        // show the results of similar practices in the past

        practicelog.load(config.filename, start_tickindex, last_tickindex, config.hand);

        rewinding = false;

        animate_scrollbar(start_tickindex);

        realtime.scroll_piece_to(start_tickindex);

        tickstats.draw();

        // after a rewind the user will have to press 'play'. This is 
        // probably not optimal in many cases; for instance listen mode
        // TODO

	if (config.mode == automatic || config.mode == mode_slide) {
                realtime.run();
	}
	else {
                realtime.stop();
	}
}

// callback function that is activated when a piece load from
// the database or from a midi file has been completed. 

function piece_loaded()

{
        // we need to save the configuration because the filename of the
        // piece we are playing has changed. 

	rewind();

	planners.autopilot_init(piece);

        realtime.stop();

        // restore the UI range slider after a piece is loaded

        if ($("#slider-range").slider("instance")) {
                $("#slider-range").slider("values", [start_tickindex, last_tickindex]);
        }
}

// TODO verify if this works properly with all keys

function vf_key_spec(keysig, scale) 

{
        if (scale == 'minor') {
                keysig = keysig + "m";
        }

        return keysig;
}

// this is a separate div, it is stationary so the one with the notes can scroll
// and the clefs and key signature remain visible.

function draw_clefs_keysig_timesig(keysig, scale, timesig)

{

        $(".clefsandsig").empty();

        // console.log(keysig, scale, timesig)

        keyspec = vf_key_spec(keysig, scale);

        var csrenderer = new VF.Renderer(document.getElementById("clefsandsig") , VF.Renderer.Backends.SVG);

        csrenderer.resize(320, 600);
        var cscontext = csrenderer.getContext();

        // fill the background of the clefs and key signature section with white,
        // that way it will eat up the notes that scroll in from the notes section
        // as they move under it.

        cscontext.setBackgroundFillStyle(config.paper);
        cscontext.clearRect(0,0,320,600);

        cscontext.scale(2,2);

        // TODO bpm is not visible; in general tempo indication gets cut off

        var cs_treble_stave = new VF.Stave(10, 60, 200).addClef('treble').addTimeSignature(timesig).addKeySignature(keyspec);
        var cs_bass_stave = new VF.Stave(10,160, 200).addClef('bass').addTimeSignature(timesig).addKeySignature(keyspec);

        // Connect it to the rendering context and draw!
        cs_treble_stave.setContext(cscontext).draw();
        cs_bass_stave.setContext(cscontext).draw();

        var connector = new VF.StaveConnector(cs_treble_stave, cs_bass_stave);

        var lineRight = new VF.StaveConnector(cs_treble_stave, cs_bass_stave).setType(0);
        var lineLeft = new VF.StaveConnector(cs_treble_stave, cs_bass_stave).setType(1);

        connector.setContext(cscontext).draw();
        lineRight.setContext(cscontext).draw();
        lineLeft.setContext(cscontext).draw();
}

// the stationary filler between the clefs and the key signature
// and the actual piece. The notes will later scroll over the filler
// all it does is to create a visual connection between the clefs and
// key signature and the first notes at the beginning of
// a piece.

function draw_filler()

{
        var w = 2000;   // wide enough to clear the right side of the window

        $(".filler").empty();

        var frenderer = new VF.Renderer(document.getElementById("filler") , VF.Renderer.Backends.SVG);

        frenderer.resize(w, 600);
        var fcontext = frenderer.getContext();

        fcontext.setBackgroundFillStyle(config.paper);
        fcontext.clearRect(0,0,w,600);

        fcontext.scale(2,2);

        var f_treble_stave = new VF.Stave(10, 60, w);
        var f_bass_stave = new VF.Stave(10,160, w);

        // Connect it to the rendering context and draw!
        f_treble_stave.setContext(fcontext).draw();
        f_bass_stave.setContext(fcontext).draw();
}

function piece_add(filename, midi)

{
	// improved this, suggested by Raphael Jakse @raphj
        if (repertoire.contains(filename)) {
                // this is a piece we already have

                console.log("already have this piece");

                piece_update_midi(filename, midi);
        }
        else {
                // new piece
                piece_midi = midi;
                config.filename = filename;

                tickstats.reset();

                // get rid of old history for this filename if we already had it
                // we don't actually want this; if there already is a set of stats
                // then we should retain them so a small update of a midi file does
                // not immediately result in the user losing all their work. 
                // piece_delete(filename);

                // maximize the range slider

                start_tickindex = 0;
                last_tickindex = 1000000;

		piece_loaded();
        }

        // show the new piece in title and repertoire drop down menu as the selected item

        repertoire.populate_file_select();
}

// this adds a piece after creating a midi object from a byte
// string that contains the contents of a midi file

function parse_midi(filename, buf) 

{
        const midi = new Midi(buf);

        // use this to add files to the built-in repertoire
	// or to create links to practices

        // console.log("midi start:");

        // console.log(JSON.stringify(midi));

        // console.log("midi end...");

        piece_add(filename, midi);
}

function play_button()

{
        realtime.run();
}

function pause_button()

{
        realtime.stop();
}

function enable(id)

{
        $(id).attr("disabled", false);
}

function disable(id) 

{
        $(id).attr("disabled", true);
}

function show_stats_counters()

{
        var total_took = Math.floor(total_practiced/100) / 10;

        // $('#statscounters').html(config.filename + " (" + total_good + " out of " + (total_good + total_errors) + ") (" + total_errors + " wrong) (" + total_late + " late) (early ???) (jitter ???? ms/note) (took " + total_took + ") delay ??? tempo ?? %;");
        $('#scorewrong').html(total_errors + "/" + (total_good + total_errors));
        $('#scorelate').html(total_late);
}


// this gets called once per second to ensure the UI is 
// in a usable state, it allows state changes to be 
// done in background tasks without having to update
// the ui all the time. 

// Doing it that way is a bit more overhead because you
// will be calling this routine more often than necessary
// even when there are no changes, but it cleans up the 
// code quite a bit and gets rid of the problem of 
// doing UI changes from deep within the real time part
// of the code.

function housekeeping()

{
        // ensure the button state is consistent with the program state

        if (realtime.is_running()) {
                enable("#pausebutton");
        }
        else {
                disable("#pausebutton");
        }

        if (realtime.is_stopped()) {
                enable("#playbutton");
        }
        else {
                disable("#playbutton");
        }

        if (realtime.get_current_tick() > 0 || realtime.is_waiting()) {
                enable("#rewind");
        }
        else {
                disable("#rewind");
        }

        // update statistics
        // technically we *could* update karma and streak here as well

        show_stats_counters();

        // TODO save the stats here when we are stopped

        // persist the program state in case the user refreshes the page or
        // navigates elsewhere; this also saves karma

        config.save();
}

// set up the file reader for the user to choose midi files locally

function read_file(e) {
        const file = e.target.files[0];
        
        // ensure the onchange event will be triggered next time,
        // even if the same file is picked again
        e.target.value = null;

        let reader = new FileReader();

        console.log("new filereader active");

        reader.onload = function(e) {

                // persist the stats of the previous file before overwriting them

                piece_save();

                console.log("reader result: ", reader.result);

                let arrayBuffer = new Uint8Array(reader.result);
                console.log(arrayBuffer);
                parse_midi(file.name, arrayBuffer);

                message_user("loaded file " + file.name);
          };

          reader.readAsArrayBuffer(file);
}

function add_file(e) {
    document.querySelector("#fileItem").click();
}

function del_file(e) {
        repertoire.del_current(config.filename);

	if (config.filename) {
		piece_retrieve(config.filename);
	}
	else {
                // no more pieces left... 
		piece = null;
		piece_midi = null;
	}

        redraw();
}

document.querySelector("#fileItem").onchange=read_file;
document.querySelector("#addmidi").onclick=add_file;
document.querySelector("#delmidi").onclick=del_file;

// rewind button

document.getElementById('rewind').addEventListener('click', (e) => {
        rewind();
});

// pause / play button

document.getElementById('playbutton').addEventListener('click', (e) => {
        play_button();
});

document.getElementById('pausebutton').addEventListener('click', (e) => {
        pause_button();
});

$( function() {
	$("#scroller").slider({
           	min: 0,
           	max: 1000,
	    	slide: function(e, ui) {
		    	scrolled(ui.value);
	    	},
	    	stop: function(e, ui) {
		    	scrolled(ui.value);
		}
        });
} );

$( function() {
	$("#speed").slider({
        	min: 10,
           	max: 200,
	   	value: tempo_multiplier*100,
           	change: speedchange,
     	});
} );

var rs_start_pos = 0;
var rs_end_pos = 1000;
var rs_delta = 1000;

$( function() {
	$( "#slider-range" ).slider({
	    range: true,
	    min: 0,
	    max: 1000,
	    values: [ 0, 1000 ],
		// the start function is used to determine how we began the slide
		// so if possible the distance can be kept constant
		start: function( event, ui ) {
			rs_start_pos = ui.values[0];
			rs_end_pos = ui.values[1];
			rs_delta = rs_end_pos - rs_start_pos;
		},
		slide: function range_slide_func( event, ui ) {
			console.log(ui.values[0], ui.values[1]);
			if (ui.values[0] != rs_start_pos) {
				// we are moving the left hand slider handle, keep the
				// right hand one at the same distance as it was at the
				// start of this slide operation

				$("#slider-range").slider("values", [ui.values[0], ui.values[0] + rs_delta]);
				practice_range_start(ui.values[0], ui.values[0] + rs_delta);
			}
			else {
				// we are moving the right hand slider handle

                        	practice_range_last(ui.values[0], ui.values[1]);
			}
		},
		stop: function( event, ui) {
			if (ui.values[0] != rs_start_pos) {
				$("#slider-range").slider("values", [ui.values[0], ui.values[0] + rs_delta]);
				practice_range_start(ui.values[0], ui.values[0] + rs_delta);
			}
			scrolled(ui.values[0]);
			rewind();
			// if we have practices this stretch before then retrieve the session logs
                        practicelog.load(config.filename, start_tickindex, last_tickindex, config.hand);
		}
	});

});

function animate_scrollbar(v)

{
        // ensure the slider is initialized or this will throw an error.
        if($("#scroller").slider("instance")) {
                $("#scroller").slider("value", v);
        }
}

function scrolled(v)

{
        scrolling = 50;         // suppress sound for the next n ticks to ensure we don't go mad from sound during scrolling

        // console.log("scroller value: ", v);

        realtime.scroll_piece_to(v);

        // ensure that scrolling through the piece from the end does not
        // re-start the practice or starts playing again in listen mode
        // maybe state = finished should go away and should alway be
        // state_stopped? clearly we are not running

        if (realtime.is_finished()) {
                realtime.stop();
        }
}

// the speed slider has moved, change the tempo and show the
// numerical representation in the display above the slider.

function speedchange(e)

{
        tempo_multiplier = $("#speed").slider("value") / 100;
        $("#speedtxt").text("Speed: " + tempo_multiplier);
}

function set_keyboardrange(l,h)

{
        config.kbd_midi_low = l;
        config.kbd_midi_high = h;

        redraw();
}

function set_keyboard_size(v)

{
        // TODO check all these with actual devices

        switch (Math.floor(v)) {        // nasty: that value from the select is a string and switch does not recognize that you are comparing strings and ints
                                        // I hate untyped languages, especially when under the hood they *do* have types!
                case 25:
                        set_keyboardrange(48,72);
                        break;
                case 32:
                        set_keyboardrange(53,84);
                        break;
                case 37:
                        set_keyboardrange(48,84);
                        break;
                case 49:
                        set_keyboardrange(36,84);
                        break;
                case 61:
                        set_keyboardrange(36,96);
                        break;
                case 73:
                        set_keyboardrange(28,100);
                        break;
                case 76:
                        set_keyboardrange(28,103);
                        break;
                case 85:
                        set_keyboardrange(21,105);
                        break;
                case 88:
                        set_keyboardrange(21,108);
                        break;
                default: 
                        console.log("unrecognized: ", v);
        }
}

function keepup_change(e)

{
        if ($("#keepup").is(":checked")) {
                config.keepup = 1;
        }
        else {
                config.keepup = 0;
        }
}

function gradespeed_change(e)

{
        if ($("#gradespeed").is(":checked")) {
                config.gradespeed = 1;
        }
        else {
                config.gradespeed = 0;
        }
}

function coupleplusplus_change(e)

{
        if ($("#coupleplusplus").is(":checked")) {
                config.coupleplusplus = 1;
        }
        else {
                config.coupleplusplus = 0;
        }
}

function coupleplus_change(e)

{
        if ($("#coupleplus").is(":checked")) {
                config.coupleplus = 1;
        }
        else {
                config.coupleplus = 0;
        }
}

function couplemanualup_change(e)

{
        if ($("#couplemanualup").is(":checked")) {
                config.couplemanualup = 1;
        }
        else {
                config.couplemanualup = 0;
        }
}

function coupleminus_change(e)

{
        if ($("#coupleminus").is(":checked")) {
                config.coupleminus = 1;
        }
        else {
                config.coupleminus = 0;
        }
}

function coupleminusminus_change(e)

{
        if ($("#coupleminusminus").is(":checked")) {
                config.coupleminusminus = 1;
        }
        else {
                config.coupleminusminus = 0;
        }
}

function notenames_change(e)

{
        if ($("#notenames").is(":checked")) {
                config.notenames = 1;
        }
        else {
                config.notenames = 0;
        }

        redraw_force = true;

        redraw();
}

function notecursors_change(e)

{
        if ($("#notecursors").is(":checked")) {
                config.notecursors = 1;
        }
        else {
                config.notecursors = 0;
        }

        redraw();
}



function keys_change(e)

{
        set_keyboard_size($("#selectKeys").val());

        message_user("Changed keyboard size to " + $("#selectKeys").val() + " keys.");
}

function hand_change()

{
        config.hand = $("#hand").val();

        redraw();

        practicelog.load(config.filename, start_tickindex, last_tickindex, config.hand);

        tickstats.draw();
}

function piece_load(filename, midi, stats, start, last)

{
        // console.log("loading ", filename);
       
        config.filename = filename; 
        piece_midi = midi;
        tickstats.set_per_tick_stats(stats);

        // these checks for undefined take care of old
        // database records, at some point they can be
        // dropped.

        if (typeof(start) != "undefined") {
                // console.log("in piece_load start ", start, " last " , last);

                start_tickindex = start;
        }
        else {
                // console.log("setting start_tickindex to 0 in piece_load");

                start_tickindex = 0;
        }

        if (typeof(last) != "undefined") {
                last_tickindex = last;
        }
        else {
                last_tickindex = 1000000;
        }

        piece_loaded();

        // show the new piece in title and repertoire drop down menu as the selected item

        repertoire.populate_file_select();
}

function file_change()

{
        // persist the stats of the previous file

        piece_save();

        piece_retrieve(repertoire.by_id(document.getElementById("selectfile").selectedIndex));
}

function practice_range_start(first, last)

{
        if (first != start_tickindex) {
                realtime.scroll_piece_to(first);
                start_tickindex = first;
		tickstats.draw();
        }
        last_tickindex = last;

        realtime.stop();

        scrolling = 50;
}

function practice_range_last(first, last)

{
        start_tickindex = first;

        if (last != last_tickindex) {
                realtime.scroll_piece_to(last);
                last_tickindex = last;
		tickstats.draw();
        }

        realtime.stop();

        scrolling = 50;
}

function load_query_parameters()

{
	// if we were called with a filename and a midi parameter then
	// load those

	const searchparams = new Proxy(new URLSearchParams(window.location.search), {
		get: (searchParams, prop) => searchParams.get(prop),
	});

	// load a midi file from the URL search parameters if one
	// was specified, this allows using pianojacq as a module
	// in another website to either link to or to embed for
	// practicing

	let param_filename = searchparams.filename;
	let param_midi = searchparams.midi; // "some_value"

	if (param_filename != null) {

        	console.log("filename parameter", param_filename);
        	console.log("midi parameter", param_midi);

        	piece_add(param_filename,JSON.parse(param_midi));

		piece_retrieve(param_filename);

		return true;
        	// console.log(window.location);
        	// debugger;
	}
	return false;
}

function db_now_connected()

{
        db_connected = true;

        config.load();

        repertoire.retrieve();
}

// we use a localstorage item in order to determine if the user
// has already ran the application before. If so then we
// will not attempt to re-initialize the repertoire

function is_firstrun()

{
        if (localStorage.getItem("alreadyran") != "1") {
                return true;
        }

        return false;
}

// TODO
// This ideally should live in repertoire somehow

var initialized_repertoire = false;
var bootstrap_timer = null;

// this routine gets called once every second if this is the
// first run of the application. Once the database is 
// connected we use it to pre-populate it with some repertoire

function bootstrap_check()

{
        // the DB needs to be alive for this to work

        if (!db_connected) {
                return;
        }

        // and we only do it once

        if (initialized_repertoire) {
                return;
        }

        // ensure the timer stops calling us

        window.clearInterval(bootstrap_timer);

        bootstrap_timer = null;

        // populate the db with some repertoire so it isn't empty

        // the repertoire is supposed to be empty right now so no need to
        // empty it again.
        // repertoire = [];

        for (var i=0;i<default_repertoire.length;i++) {
                piece_add(default_repertoire[i].filename, JSON.parse(default_repertoire[i].midi));
        }

        // and set the cookie so we won't come back here again

        localStorage.setItem("alreadyran", "1");

        initialized_repertoire = true;
}

// Set of functions to show and hide settings menu
// maybe this should go to config.js?
var nav_open = false
function toggleNav() {
        navSize = document.getElementById("topmenuwrapper").style.height;
        if (nav_open) {
	        nav_open = false;
                return closeNav();
        }
        nav_open = true
        return openNav();
}

function openNav() {
        var height = document.getElementById("play-container").offsetHeight
                + document.getElementsByClassName("repertoire")[0].offsetHeight + "px"
        document.getElementById("topmenuwrapper").style.height = height;
        document.getElementById("insideBar").style.height = height;
        document.getElementById("topmenuwrapper").style.width = "100%";

}

function closeNav() {
        document.getElementById("topmenuwrapper").style.height = "80px";
        document.getElementById("insideBar").style.height = "80px";
}

// catch clicks to enable Tone.js (a requirement in chrome is to interact
// in a conscious way with an application before it will be allowed to 
// make sounds, this makes good sense in general but is kind of annoying
// for an app whose sole purpose is to help make sound).

// This captures any clicks anywhere in the doc. The idea
// here is that it will help to get Tone.js started if
// that is out preferred output method without an explicit
// Tone.js start button

function any_click()

{
        midiio.start_tone_js();
}

// toggle the main tabs on and off by making only one visible at the time


function hidealltabs()

{
        $( '#tabsettingswrapper' ).hide();
        $( '#tabsightreadingwrapper' ).hide();
        $( '#tabeartrainingwrapper' ).hide();
        $( '#tabsoundguesserwrapper' ).hide();
        $( '#tabmusictheorywrapper' ).hide();
}

function tabSettings()

{
        hidealltabs();
        $( '#tabsettingswrapper' ).show();
        current_tab = tabsettings;
}

function tabSightreading()

{
        hidealltabs();
        $( '#tabsightreadingwrapper' ).show();
        current_tab = tabsightreading;
}

function tabEartraining()

{
        hidealltabs();
        $( '#tabeartrainingwrapper' ).show();
        current_tab = tabeartraining;

        et_start();
}

function tabSoundguesser()

{
        hidealltabs();
        $( '#tabsoundguesserwrapper' ).show();
        current_tab = tabsoundguesser;

        sg_start();
}

function tabTheory()

{
        hidealltabs();
        $( '#tabmusictheorywrapper' ).show();
        current_tab = tabtheory;
}

$( '#mainTab' ).hide();

tabSightreading();

document.body.addEventListener('click', any_click, true);

// main program starts here

// do we want this declaration in index.js? piece.js would seem more appropriate.

VF = Vex.Flow;

rewind();

if (is_firstrun()) {
        bootstrap_timer = window.setInterval(bootstrap_check, 1000);
}

// call the housekeeping routine twice every second

window.setInterval(housekeeping, 500);

// keyboard bindings to help with navigation
// space plays or stops the piece, left arrow rewinds.
$("body").on("keydown", function(e){
        if(e.which == 32 && $("#playbutton").is(":disabled")) $("#pausebutton").click();
        if(e.which == 32 && $("#pausebutton").is(":disabled")) $("#playbutton").click();
        if(e.which == 37 && $("#rewind").is(":enabled")) $("#rewind").click();
});


